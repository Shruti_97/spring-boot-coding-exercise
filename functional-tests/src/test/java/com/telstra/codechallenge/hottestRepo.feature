Feature: As a developer i want to test the hottestRepositories uri

  Scenario: Is the hottestRepositories uri available and functioning
    Given url microserviceUrl
    And path '/hottestRepositories' + '3'
    When method GET
    Then status 200
    And match header Content-Type contains 'application/json'
    # see https://github.com/intuit/karate#schema-validation
    *def quoteSchema
    { type : success ,
      value : 
      { 
      	html_url : '#string',
      	name : '#string',
      	description : '#string',
      	watchers_count : '#number,
      	language : '#string'
      }      	
    }
    And match response == '#[12] quoteSchema' 