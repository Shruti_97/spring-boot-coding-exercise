package com.telstra.codechallenge.hottestRepo.service;

import java.util.List;

import com.telstra.codechallenge.hottestRepo.model.Item;

public interface HottestRepositoryService {

	List<Item> getHottestRepositoriesInformation(int numberOfRepoInfoToReturn);
}
