package com.telstra.codechallenge.hottestRepo.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.telstra.codechallenge.hottestRepo.model.Item;
import com.telstra.codechallenge.hottestRepo.service.HottestRepositoryService;
import com.telstra.codechallenge.hottestRepo.service.HottestRepositoryServiceImpl;


@RestController
public class HottestRepositoryController {
	
	
	private HottestRepositoryService hottestRepoService;
	
	private Logger logger = LoggerFactory.getLogger(HottestRepositoryController.class);
	
	@Autowired
	public  HottestRepositoryController(HottestRepositoryServiceImpl hottestRepoService) {
		this.hottestRepoService = hottestRepoService;		
	}
	
	@GetMapping("/hottestRepositories/{numOfRepositoriesToReturn}")
	public List<Item> getHottestRepositories(@PathVariable int numOfRepositoriesToReturn) {
		return hottestRepoService.getHottestRepositoriesInformation(numOfRepositoriesToReturn);
	}	
}
