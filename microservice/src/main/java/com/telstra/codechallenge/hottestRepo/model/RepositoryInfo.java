package com.telstra.codechallenge.hottestRepo.model;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.telstra.codechallenge.MicroServiceMain;

public class RepositoryInfo {
	
	private long total_count;
	private boolean incomplete_results;
	private ArrayList<Item> items;
	
	public long getTotal_count() {
		return total_count;
	}
	public void setTotal_count(long total_count) {
		this.total_count = total_count;
	}
	public boolean getIfIncomplete_results() {
		return incomplete_results;
	}
	public void setIncomplete_results(boolean incomplete_results) {
		this.incomplete_results = incomplete_results;
	}
	public ArrayList<Item> getItems() {
		return items;
	}
	public void setItems(ArrayList<Item> repositories) {
		this.items = repositories;
	}

}
