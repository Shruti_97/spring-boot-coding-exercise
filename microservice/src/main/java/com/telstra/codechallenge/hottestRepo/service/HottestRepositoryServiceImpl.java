package com.telstra.codechallenge.hottestRepo.service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.telstra.codechallenge.MicroServiceMain;
import com.telstra.codechallenge.hottestRepo.model.Item;
import com.telstra.codechallenge.hottestRepo.model.RepositoryInfo;

@Service
public class HottestRepositoryServiceImpl implements HottestRepositoryService {

	private Logger logger = LoggerFactory.getLogger(MicroServiceMain.class);
	
	@Value("${hottestRepo.base.url}")
	private String hottestRepoBaseUrl;

	private RestTemplate restTemplate;

	@Autowired
	public HottestRepositoryServiceImpl( RestTemplate restTemplate ) {
	  this.restTemplate = restTemplate; 
	}
	 

	@Override
	public List<Item> getHottestRepositoriesInformation(int numberOfRepoInfoToReturn) {
		LocalDate currentDate = LocalDate.now();
		logger.info(" Request URL " + hottestRepoBaseUrl+ "?q=created:" + currentDate.minusDays(7).toString() + "&sort=stars&order=desc");
		RepositoryInfo repositories = restTemplate.getForObject(
				hottestRepoBaseUrl + "?q=created:" + currentDate.minusDays(7).toString() + "&sort=stars&order=desc",
				RepositoryInfo.class);
		return repositories.getItems().stream().limit(numberOfRepoInfoToReturn).collect(Collectors.toList());
	}

}
