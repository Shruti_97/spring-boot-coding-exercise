package com.telstra.codechallenge.controller;


import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import com.telstra.codechallenge.hottestRepo.controller.HottestRepositoryController;
import com.telstra.codechallenge.hottestRepo.model.Item;
import com.telstra.codechallenge.hottestRepo.service.HottestRepositoryService;

@RunWith(MockitoJUnitRunner.class)
public class HottestRepositoryControllerTest {
 	
	
	@Spy
	private HottestRepositoryService hottestRepositoryService;
	
	@InjectMocks
	private HottestRepositoryController hottestRepositoryController;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void getHottestRepositoryTest() {
		ArrayList<Item> testRepos = new ArrayList<>();
		Item item = new Item();
		testRepos.add(item);
		testRepos.add(item);
		testRepos.add(item);
		Mockito.when(hottestRepositoryService.getHottestRepositoriesInformation(3)).thenReturn(testRepos);
		List<Item> items = hottestRepositoryController.getHottestRepositories(3);
		Assert.assertEquals(items.size(),3);
	}
	
	@Test
	public void testIfNoReposAreReturnedForZero() {
		ArrayList<Item> testRepos = new ArrayList<>();
		Mockito.when(hottestRepositoryService.getHottestRepositoriesInformation(0)).thenReturn(testRepos);
		List<Item> repos = hottestRepositoryController.getHottestRepositories(0);
		Assert.assertTrue(repos.isEmpty());
	}
	
	@Test(expected=Exception.class)
	public void testExceptionIsThrown_WhenNegativeRepoCount() {
		Mockito.when(hottestRepositoryService.getHottestRepositoriesInformation(-2)).thenThrow(Exception.class);
		List<Item> repos = hottestRepositoryController.getHottestRepositories(-2);
	}
}
