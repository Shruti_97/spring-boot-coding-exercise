package com.telstra.codechallenge.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import com.telstra.codechallenge.hottestRepo.model.Item;
import com.telstra.codechallenge.hottestRepo.model.RepositoryInfo;
import com.telstra.codechallenge.hottestRepo.service.HottestRepositoryServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class HottestRepositoryServiceTest{
	
	@Spy
	RestTemplate restTemplate ;
	
	@InjectMocks
	HottestRepositoryServiceImpl hottestRepoService;
		
	private String repoSearchUrl = "https://api.github.com/search/repositories";
	
	@Before
	public void setUp() {
		ReflectionTestUtils.setField(hottestRepoService, "hottestRepoBaseUrl","https://api.github.com/search/repositories" );
	}
	
	@Test
	public void testIfReturnedRepositoriesAreExpectedNumbers() {
		ArrayList<Item> testRepos = new ArrayList<Item>();
		Item item = new Item();
		testRepos.add(item);
		testRepos.add(item);
		RepositoryInfo repoInfo = new RepositoryInfo();
		repoInfo.setItems(testRepos);
		LocalDate currentDate = LocalDate.now();
		Mockito.when(restTemplate.getForObject(repoSearchUrl+"?q=created:" + currentDate.minusDays(7).toString() + "&sort=stars&order=desc",
				RepositoryInfo.class)).thenReturn(repoInfo);
		List<Item> list = hottestRepoService.getHottestRepositoriesInformation(2);
		Assert.assertEquals(list.size(), 2);
	}
	
	@Test
	public void testIfNoReposAreReturnedForZero() {
		ArrayList<Item> testItems = new ArrayList<>();
		RepositoryInfo repoInfo = new RepositoryInfo();
		repoInfo.setItems(testItems);
		LocalDate currentDate = LocalDate.now();
		Mockito.when(restTemplate.getForObject(repoSearchUrl+"?q=created:" + currentDate.minusDays(7).toString() + "&sort=stars&order=desc",
				RepositoryInfo.class)).thenReturn(repoInfo);
		List<Item> items = hottestRepoService.getHottestRepositoriesInformation(0);
		Assert.assertTrue(items.isEmpty());
	}
	
	
	
	
}
