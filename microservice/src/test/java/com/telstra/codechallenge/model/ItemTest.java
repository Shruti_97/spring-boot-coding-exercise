package com.telstra.codechallenge.model;

import org.junit.Assert;
import org.junit.Test;
import com.telstra.codechallenge.hottestRepo.model.Item;

public class ItemTest {

	@Test
	public void testGetterAndSetterHTML_URL() {
		Item item = new Item();
		item.setHtml_url("https://github.com");
		Assert.assertEquals(item.getHtml_url(),"https://github.com");
	}
	
	@Test
	public void testGetterAndSetterWatchersCount() {
		Item item = new Item();
		item.setWatchers_count(5);
		Assert.assertEquals(item.getWatchers_count(), 5);
	}
	
	@Test
	public void testGetterAndSetterLanguage() {
		Item item = new Item();
		item.setLanguage("en");
		Assert.assertEquals(item.getLanguage(), "en");
	}
	
	@Test
	public void testGetterAndSetterDescription() {
		Item item = new Item();
		item.setDescription("This is a testing repository");
		Assert.assertEquals(item.getDescription(), "This is a testing repository");
	}

	@Test
	public void testGetterAndSetterName() {
		Item item = new Item();
		item.setName("Test Repo");
		Assert.assertEquals(item.getName(), "Test Repo");
	}
}
