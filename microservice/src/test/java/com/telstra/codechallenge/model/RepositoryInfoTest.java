package com.telstra.codechallenge.model;

import java.util.ArrayList;
import org.junit.Assert;
import org.junit.Test;
import com.telstra.codechallenge.hottestRepo.model.Item;
import com.telstra.codechallenge.hottestRepo.model.RepositoryInfo;

public class RepositoryInfoTest {

	@Test
	public void testGetterAndSetterTotalCount() {
		RepositoryInfo repoInfo = new RepositoryInfo();
		repoInfo.setTotal_count(5);
		Assert.assertEquals(repoInfo.getTotal_count(),5);
	}
	
	@Test
	public void testGetterAndSetterIncompleteResult() {
		RepositoryInfo repoInfo = new RepositoryInfo();
		repoInfo.setIncomplete_results(false);
		Assert.assertEquals(repoInfo.getIfIncomplete_results(),false);
	}
	
	@Test
	public void testGetterAndSetterRepoItem() {
		RepositoryInfo repoInfo = new RepositoryInfo();
		ArrayList<Item> items = new ArrayList<>();
		Item item = new Item();
		items.add(item);
		repoInfo.setItems(items);
		Assert.assertSame(repoInfo.getItems(),items);
	}
	
}
